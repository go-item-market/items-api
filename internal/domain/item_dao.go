package domain

import (
	"fmt"
)

var (
	storage map[string]Item = make(map[string]Item)
)

// Get item from storage
func (item *Item) Get() error {
	if err := item.Validate(); err != nil {
		return err
	}
	it, ok := storage[item.ID]
	if !ok {
		return fmt.Errorf("Item of id %v not found", item.ID)
	}
	*item = *&it
	return nil
}

// Save item to storage
func (item *Item) Save() error {
	if err := item.Validate(); err != nil {
		return err
	}
	storage[item.ID] = *item
	return nil
}
