package domain

import (
	"fmt"
	"strings"
)

// Item data object
type Item struct {
	ID          string
	Price       float32
	Seller      string
	Description string
}

// Validate correctness of Item object
func (item *Item) Validate() error {
	if strings.TrimSpace(item.ID) == "" {
		return fmt.Errorf("Invalid item id %s", item.ID)
	}
	return nil
}
