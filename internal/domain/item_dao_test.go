package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestItem_Correct(t *testing.T) {
	valid := Item{
		ID:          "1",
		Seller:      "ABC",
		Price:       100.5,
		Description: "Desc",
	}
	assert.Error(t, valid.Get(), "Should not be found")
	assert.NoError(t, valid.Save(), "No error should occur")

	itemID := Item{ID: "1"}
	assert.NoError(t, itemID.Get(), "Item should exist")
	assert.Equal(t, valid, itemID, "Identical")
}

func TestItem_InvalidID(t *testing.T) {
	invalidID := Item{ID: "   "}
	assert.Error(t, invalidID.Get(), "Error")
	assert.Error(t, invalidID.Save(), "Error")
}
