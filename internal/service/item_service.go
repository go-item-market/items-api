package service

import "gitlab.com/go-item-market/items-api/internal/domain"

var (
	// ItemService interface for accessing item service
	ItemService itemServiceInterface = &itemService{}
)

type itemServiceInterface interface {
	GetItem(string) (*domain.Item, error)
	SaveItem(domain.Item) (*domain.Item, error)
}

type itemService struct{}

func (service *itemService) GetItem(itemID string) (*domain.Item, error) {
	item := &domain.Item{ID: itemID}
	if err := item.Get(); err != nil {
		return nil, err
	}
	return item, nil
}

func (service *itemService) SaveItem(item domain.Item) (*domain.Item, error) {
	if err := item.Save(); err != nil {
		return nil, err
	}
	return &item, nil
}
