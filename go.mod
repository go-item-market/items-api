module gitlab.com/go-item-market/items-api

go 1.15

require (
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/micro/go-micro/v2 v2.9.1
	github.com/stretchr/testify v1.4.0
	google.golang.org/protobuf v1.23.0 // indirect
)
